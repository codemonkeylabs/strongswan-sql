{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes #-}

module CLI.Commands.TrafficSelector where

import Control.Lens                        (Lens', (.=), (%=), use,
                                            partsOf, singular, _head,
                                            view, to)
import Control.Monad                       (when, forM, void)
import Control.Monad.IO.Class              (MonadIO, liftIO)
import CLI.Commands.Common
import CLI.Types
import Data.Default                        (def)
import Data.Functor                        ((<&>))
import Data.Maybe                          (fromMaybe)
import Control.Monad.State.Strict          (StateT, lift)
import StrongSwan.SQL
import System.Console.StructuredCLI hiding (Commands)
import Text.Read                           (readMaybe)

trafficSelectorType :: (Monad m) => Validator m TrafficSelectorType
trafficSelectorType = return . fromName

parseTSId :: (Monad m) => Validator m (Maybe Int)
parseTSId "new" = return $ Just Nothing
parseTSId str   = return $ Just <$> readMaybe str

cfgTrafficSelectors :: Lens' IPSecSettings [TrafficSelector] -> Commands ()
cfgTrafficSelectors lens = do
      command "traffic-selector" "Configure remote traffic selector" newLevel >+ do
        command "add" "Add traffic selector" (addTrafficSelector lens Nothing) >+ do
          cfgTrafficSelector (lens . singular _head)
          command "show" "Display traffic selector's parameters" $
            showTrafficSelector (lens . singular _head)
          exitCmd
        command "list" "Display traffic selectors" $
          showTrafficSelectors' lens
        param "remove" "<existing traffic selector ID>" integer $
          deleteTrafficSelector lens
        exitCmd

cfgTrafficSelector :: Lens' IPSecSettings TrafficSelector -> Commands ()
cfgTrafficSelector lens = do
  cfgTSType lens
  cfgTSProtocol lens
  cfgTSStartAddr lens
  cfgTSEndAddr lens
  cfgTSStartPort lens
  cfgTSEndPort lens

setTrafficSelector :: Maybe Int -> StateT AppState IO Action
setTrafficSelector mId = do
  flush .= Just (flushTrafficSelector getLocalTrafficSelectors)
  addTrafficSelector getLocalTrafficSelectors mId

addTrafficSelector :: Lens' IPSecSettings [TrafficSelector] -> Maybe Int -> StateT AppState IO Action
addTrafficSelector lens mId = do
  ts <- case mId of
          Nothing ->
            return def
          Just iD -> do
            db <- use dbContext
            liftIO $ findTrafficSelector iD db
  ipsecSettings . lens %= (ts:)
  return NewLevel

deleteTrafficSelector :: Lens' IPSecSettings [TrafficSelector] -> Int -> StateT AppState IO Action
deleteTrafficSelector lens iD = do
  ipsecCfg <- use ipsecSettings
  db       <- use dbContext
  void $ liftIO $ deleteIPSecSettings ipsecCfg db
  ipsecSettings . lens %= filter ( view $ tsId . to (Just iD /=) )
  flushIt

showTrafficSelectors :: Commands ()
showTrafficSelectors =
  command "show" "Display this traffic selector's parameters" $
    showTrafficSelectors' getLocalTrafficSelectors

showTrafficSelectors' :: Lens' IPSecSettings [TrafficSelector] -> StateT AppState IO Action
showTrafficSelectors' lens = do
  use (ipsecSettings . lens) >>= mapM_ showTrafficSelector'
  return NoAction

showTrafficSelector :: Lens' IPSecSettings TrafficSelector -> StateT AppState IO Action
showTrafficSelector lens = do
  use (ipsecSettings . lens) >>= showTrafficSelector'
  return NoAction

showTrafficSelector' :: (MonadIO m) => TrafficSelector -> m ()
showTrafficSelector' TrafficSelector{..} = do
  let iD = _tsId <&> show
  liftIO $ do
    putStr "Traffic Selector "
    putStrLn $ "(ID: " ++ fromMaybe "*uncommitted*" iD ++ ")"
    putStrLn $ "==================================";
    putStrLn $ "Type:          " ++ nameOf _tsType
    putStrLn $ "Protocol:      " ++ show _tsProtocol
    putStrLn $ "Start address: " ++ show _tsStartAddr
    putStrLn $ "End address:   " ++ show _tsEndAddr
    putStrLn $ "Start port:    " ++ show _tsStartPort
    putStrLn $ "End port:      " ++ show _tsEndPort

cfgTSType :: Lens' IPSecSettings TrafficSelector -> Commands ()
cfgTSType lens =
    param "type" "<ipv4|ipv6>" trafficSelectorType $ \t -> do
      ipsecSettings . lens . tsType .= t
      flushIt

cfgTSProtocol :: Lens' IPSecSettings TrafficSelector -> Commands ()
cfgTSProtocol lens =
    param "protocol" "<protocol number>" integer $ \val -> do
      ipsecSettings . lens . tsProtocol .= val
      flushIt

cfgTSStartAddr :: Lens' IPSecSettings TrafficSelector -> Commands ()
cfgTSStartAddr lens =
    param "start-address" "<first IP address in range>" ipAddress $ \addr -> do
      ipsecSettings . lens .tsStartAddr .= addr
      flushIt

cfgTSEndAddr :: Lens' IPSecSettings TrafficSelector -> Commands ()
cfgTSEndAddr lens =
    param "end-address" "<last IP address in range>" ipAddress $ \addr -> do
      ipsecSettings . lens . tsEndAddr .= addr
      flushIt

cfgTSStartPort :: Lens' IPSecSettings TrafficSelector -> Commands ()
cfgTSStartPort lens =
    param "start-port" "<first L4 port number in range>" integer $ \port -> do
      ipsecSettings . lens . tsStartPort .= port
      flushIt

cfgTSEndPort :: Lens' IPSecSettings TrafficSelector -> Commands ()
cfgTSEndPort lens =
    param "end-port" "<last L4 port number in range>" integer $ \port -> do
      ipsecSettings . lens . tsEndPort .= port
      flushIt

flushTrafficSelector :: Lens' IPSecSettings [TrafficSelector] -> StateT AppState IO Action
flushTrafficSelector lens = do
    tss  <- use (ipsecSettings . lens)
    db   <- use dbContext
    keys <- forM tss $ \ ts -> do
      Result {response = OK {..}, ..} <- lift $ writeTrafficSelector ts db
      when (okAffectedRows /= 1 ) $
        liftIO . putStrLn $ "(1) warning: affected " ++ show okAffectedRows ++ " (expected 1)"
      return $ Just lastModifiedKey
    partsOf (ipsecSettings . lens . traverse . tsId) .= keys
    return NoAction
