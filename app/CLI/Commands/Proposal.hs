{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes #-}

module CLI.Commands.Proposal where

import Control.Lens                        (Lens', (.=), (%=), use,
                                            partsOf, singular, _head,
                                            view, to)
import Control.Monad                       (when, forM, void)
import Control.Monad.IO.Class              (MonadIO, liftIO)
import CLI.Commands.Common
import CLI.Types
import Data.Default                        (def)
import Data.Functor                        ((<&>))
import Data.Maybe                          (fromMaybe)
import Data.Text                           (unpack)
import Control.Monad.State.Strict          (StateT, lift)
import StrongSwan.SQL
import System.Console.StructuredCLI hiding (Commands)
import Text.Read                           (readMaybe)

parsePropId :: (Monad m) => Validator m (Maybe Int)
parsePropId "new" = return $ Just Nothing
parsePropId str   = return $ Just <$> readMaybe str

cfgProposals :: Lens' IPSecSettings [Proposal] -> Commands ()
cfgProposals lens = do
      command "proposal" "Configure encryption proposals" newLevel >+ do
        command "add" "Add encryption proposal" (addProposal lens Nothing) >+ do
          cfgProposal (lens . singular _head)
          command "show" "Display proposal's parameters" $
            showProposal (lens . singular _head)
          exitCmd
        command "list" "Display proposals" $
          showProposals' lens
        param "remove" "<existing proposal ID>" integer $
          deleteProposal' lens
        exitCmd

cfgProposal :: Lens' IPSecSettings Proposal -> Commands ()
cfgProposal lens = do
  cfgProposalStr lens

setProposal :: Lens' IPSecSettings [Proposal] -> Maybe Int -> StateT AppState IO Action
setProposal lens mId = do
  flush .= Just (flushProposal lens)
  addProposal lens mId

addProposal :: Lens' IPSecSettings [Proposal] -> Maybe Int -> StateT AppState IO Action
addProposal lens mId = do
  prop <- case mId of
          Nothing ->
            return def
          Just iD -> do
            db <- use dbContext
            liftIO $ findProposal iD db
  ipsecSettings . lens %= (prop:)
  return NewLevel

deleteProposal' :: Lens' IPSecSettings [Proposal] -> Int -> StateT AppState IO Action
deleteProposal' lens iD = do
  ipsecCfg <- use ipsecSettings
  db       <- use dbContext
  void $ liftIO $ deleteIPSecSettings ipsecCfg db
  ipsecSettings . lens %= filter ( view $ pId . to (Just iD /=) )
  flushIt

showProposals :: Commands ()
showProposals =
  command "show" "Display this traffic selector's parameters" $
    showProposals' getChildProposals

showProposals' :: Lens' IPSecSettings [Proposal] -> StateT AppState IO Action
showProposals' lens = do
  use (ipsecSettings . lens) >>= mapM_ showProposal'
  return NoAction

showProposal :: Lens' IPSecSettings Proposal -> StateT AppState IO Action
showProposal lens = do
  use (ipsecSettings . lens) >>= showProposal'
  return NoAction

showProposal' :: (MonadIO m) => Proposal -> m ()
showProposal' Proposal{..} = do
  let iD = _pId <&> show
  liftIO $ do
    putStr "Proposal "
    putStrLn $ "(ID: " ++ fromMaybe "*uncommitted*" iD ++ ")"
    putStrLn $ "==================================";
    putStrLn $ "Proposal string:          " ++ unpack _pProposal

cfgProposalStr :: Lens' IPSecSettings Proposal -> Commands ()
cfgProposalStr lens =
    param "type" "<Encryption proposal>" string $ \p -> do
      ipsecSettings . lens . pProposal .= p
      flushIt

flushProposal :: Lens' IPSecSettings [Proposal] -> StateT AppState IO Action
flushProposal lens = do
    props  <- use (ipsecSettings . lens)
    db   <- use dbContext
    keys <- forM props $ \ prop -> do
      Result {response = OK {..}, ..} <- lift $ writeProposal prop db
      when (okAffectedRows /= 1 ) $
        liftIO . putStrLn $ "(1) warning: affected " ++ show okAffectedRows ++ " (expected 1)"
      return $ Just lastModifiedKey
    partsOf (ipsecSettings . lens . traverse . pId) .= keys
    return NoAction
